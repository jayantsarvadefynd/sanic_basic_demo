from sanic import Sanic, json
from sanic import response
from sanic.log import logger
from controller import my_blueprint
import traceback
from sanic.exceptions import ServerError, NotFound


app= Sanic(name= "first")

app.blueprint(my_blueprint)

app.static('/python.jpeg','sanic_demo/sanic_demo/python.jpeg')

# To handle exception

@app.route('/timeout')
async def terminate(request):
    raise ServerError("Gateway Timeout error", status_code = 504)


@app.exception(NotFound)
async def ignore_5xx(request, exception):
    return response.text(f"Gateway is always up: {request.url}")

    

# to display hello world
@app.route("/")
def run(request):
    return response.text("Hello this is new user")

# to render template/html page
@app.route("/display")
def display(request):
    return response.file('sanic_demo/sanic_demo/index.html')



# post request
@app.route("/post", methods =['POST'])
def on_post(request):
    try:
        return response.json({"content": request.json})
    except Exception as ex: 
        logger.error(f"{traceback.format_exc()}")

name_list=[]

# get request
@app.route("/get", methods =['GET'])
def on_post(request):
    try:
        data=request.json
        names=(data["name"],data["age"],data["roll"])
        name_list.append(names)
        return json(names, status=201)
        
    except Exception as ex: 
        logger.error(f"{traceback.format_exc()}")






app.run(host="0.0.0.0", port= "8000", debug= True)

