from sanic import response
from sanic import Blueprint

my_blueprint = Blueprint('my_blueprint')

@my_blueprint.route('/my_blueprint')
def my_blueprint_func(request):
	return response.text('My First Blueprint')
