from sanic import Sanic
from sanic.response import json


app=Sanic("sanic_demo")
cat_list=[]
class Cat(object):
    def __init__(self, name,age,height):
        self.name= name
        self.age= age
        self.height= height
    def to_json(self):
        return {
            "name": self.name,
            "age" : self.age,
            "height": self.height

        }

@app.route("create_cat", method = ["POST"])
async def create_cat(request):
    try:
        data= request.json
        cat= Cat(data["name"], data["age"],data["height"] )
        cat_list.append(cat)
        return json(cat, status= 201)
    except Exception as error:
        print(error)
        return json({"message": "cat creation failed"}, status= 500)

if __name__ == "__main__":
    app.run(host= "0.0.0.0", port= 8000)